  window.onload = function() {
    document.querySelector(docjs_config.element).insertAdjacentHTML("afterend", "<div id='DOCJS_SIDEBAR'></div>");
    //document.getElementById('DOCJS_SIDEBAR').innerHTML = 1;
    fetch("https://gitcdn.link/repo/" + docjs_config.username + "/" +  docjs_config.sidebar)
      .then(response => response.text())
      .then(data => {
      document.getElementById('DOCJS_SIDEBAR').innerHTML = marked(data);
    })
      .catch(error => console.error(error))
  }
