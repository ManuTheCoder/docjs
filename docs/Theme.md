### Theme
To change the theme to light, add this parameter:
```
lightTheme: true
```

Full example:
```
var docjs_config = {
  username: 'ManuTheCoder', 
  repo_name: 'docjs',
  sidebar: 'docjs/main/docs/sidebar.md', 
  element: '#main',
  title: 'DocJS', 
  github_square: true,
  navbar_enabled: true, 
  path: '/docs',
  logo: 'https://img.icons8.com/color/2x/documents.png',
  lightTheme: true
}
```
