### Markdown Syntax guide

# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
##### Heading 6
```
# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
##### Heading 6
```
Paragraph
[Link](#)
Inline `code`
```
Paragraph
[Link](#)
Inline `code`
```
```
// Block code
var foo = 1;
var bar = 2
```
> Blockquote

```
Horizontal rule

---

Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~

```
Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~
```
