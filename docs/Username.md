### Username
The username parameter is used to specify the user. This is optional, if you haven't enabled the "github_square" feature. 
#### Usage
```
username: 'ManuTheCoder',
```
