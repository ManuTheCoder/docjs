## Initialization
To initalize the code, you'll need to include the files. <br> 
Insert this code in your index.html file:
```

<!DOCTYPE html>
<html>
	<head>
		<title>Docs</title>
    <link rel="stylesheet" href="./src/style.css">
		<link rel='stylesheet' href='https://yandex.st/highlightjs/8.0/styles/vs.min.css'>
	</head>
	<body>
		<div id="main"></div>
		<script src='https://yandex.st/highlightjs/8.0/highlight.min.js'></script>
		<script>
		  var docjs_config = {
		    username: 'ManuTheCoder', 
		    repo_name: 'docjs',
		    sidebar: 'docjs/main/docs/sidebar.md', 
		    element: '#main',
		    title: 'DocJS'
		  }
			  hljs.initHighlightingOnLoad();
		window.onerror = function(msg, url, linenumber) {
		    alert('Error message: '+msg+'\nURL: '+url+'\nLine Number: '+linenumber);
		    return true;
		}
		</script>
    <script src="./src/script.js"></script>
	</body>
</html>

```
