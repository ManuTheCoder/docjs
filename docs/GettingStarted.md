# Getting Started
<br>

#### CDN
```
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/ManuTheCoder/docjs/docs/src/style.css">
<script src="https://cdn.jsdelivr.net/gh/ManuTheCoder/docjs/docs/src/script.js"></script>
```
It's easy to get started! 
* Download the files from the github repository: [https://github.com/ManuTheCoder/docjs](https://github.com/ManuTheCoder/docjs)
* Create a new repository named "docs" or whatever you want!
* Create a file named index.html
#### Template
```

<!DOCTYPE html>
<html>
	<head>
		<title>Docs</title>    <title>Docs</title><link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="./src/style.css">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
		<style>
			*:not(code) {font-family: 'Lato', sans-serif;}
		</style>
	</head>
	<body>
		<div id="main"></div>
		<script>
            var docjs_config = {
          domain: 'https://raw.githubusercontent.com/ManuTheCoder/docjs/main/docs/',
          sidebar: 'docjs/main/docs/sidebar.md', 
          element: '#main',
          title: 'DocJS', 
          username: 'ManuTheCoder',
          repo_name: 'docjs',
          github_square: true,
          navbar_enabled: true, 
          path: '/docs',
          logo: 'https://img.icons8.com/color/2x/documents.png',
          lightTheme: true,
          switchMode: true,
	  bottom_nav: true,
		    copyToClipboard: true,
            }
		window.onerror = function(msg, url, linenumber) {
		    alert('Error message: '+msg+'\nURL: '+url+'\nLine Number: '+linenumber);
		    return true;
		}
		</script>
		<script src="https://cdn.polyfill.io/v2/polyfill.js?features=fetch"></script>
    <script src="./src/script.js"></script>
	</body>
</html>

```
