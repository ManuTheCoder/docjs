### Copy to clipboard
The copyToClipboard parameter when true, it enables copying code elements when clicked on

#### Usage
```
copyToClipboard: true,
```
