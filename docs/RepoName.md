### Repo name
This parameter is used to specify the repository name. This is optional, but required if you have enabled the "github_square" feature
#### Usage
```
repo_name: 'docjs',
```
