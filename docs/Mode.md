### Mode
The mode parameter is used to specify the mode (Markdown, or HTML mode)

#### Usage
```
mode: 'html'
```
