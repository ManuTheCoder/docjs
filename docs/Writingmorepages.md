### Writing more pages
To write more pages, you need to first add a link in the sidebar (Example: About). Then, create a file named About.md (with an uppercase A). 

If you have enabled HTML mode, create a file named "About.html". For multiple words, just trim the spaces in the filenames (Example: GettingStarted.md).

#### Example file structure
```
... (Home directory)
└── docs
    ├── README.md
    ├── WritingMorePages.md
    ├── About.md
    ├── Contact.md
    ├── News.md
    └── guide.md
```
