### Navbar
The navbar_enabled parameter is used to add/remove a navbar
To add links to a navbar, create a file named navbar.md or navbar.html (Depending on the mode)
#### Usage
```
navbar_enabled: true, 
```
