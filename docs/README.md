# DocJS
A Front end Documentation generator with no external dependencies

[GitHub](https://github.com/ManuTheCoder/docjs/)<br>
[About the developer](https://manuthecoder.ml/)<br>
[Other cool apps](https://smartlist.ga/)<br>


### How does it work? 
DocJS uses very minimalistic code to make a beautiful single-page documentation page. DocJS relies on markdown for formatting, making it easier to write pages! You can also use HTML instead if you wish<br>
You can easily host this type of documentation on GitHub pages, or if you want on your hosting provider. See the "Custom Host" section for more details. 

### Feautures
* Search
* Blazing fast pages!
* Github logo in corner
* Custom sidebar
* Markdown processing 
* Custom domain
* Easy to create!!!
