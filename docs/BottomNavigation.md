### Bottom Navigation
The bottom_nav parameter is used to specify whether the bottom nav appears or not. 

### Usage
```
bottom_nav: true,
```
|Options  |
|---------|
|  True  |
|False|
