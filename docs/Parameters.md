### Parameters
Click on the next few links to view parameters. Parameters go in the var docjs_config section

#### Usage
```
var docjs_config = {
  domain: 'https://docjs.manuthecoder.repl.co/',
  sidebar: 'docjs/main/docs/sidebar.md', 
  element: '#main',
  title: 'DocJS', 
  username: 'ManuTheCoder',
  repo_name: 'docjs',
  github_square: true,
  navbar_enabled: true, 
  path: '/docs',
  logo: 'https://img.icons8.com/color/2x/documents.png',
  lightTheme: true,
  switchMode: true,
  bottom_nav: true,
  copyToClipboard: true,
}
 ```
