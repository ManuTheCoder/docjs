### Element
The element parameter is used to specify which element the documentation loads. <br>
Specify it like this:
```
element: '#main'
```
