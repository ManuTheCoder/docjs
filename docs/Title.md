### Title
This parameter is used to specify the title of the documentation. This is required
#### Usage
```
title: 'DocJS',
```
